# Makefile for orb-engine
# Compiles the code for the engine
# Created on 6/18/2018
# Created by Andrew Davis
#
# Copyright (C) 2018  Andrew Davis
#
# Licensed under the MIT License

# define the compiler
CXX=g++

# define the compiler flags for the library
CXXFLAGS=-fPIC -c -Wall -std=c++17

# define the compiler flags for the test program
test: CXXFLAGS=-c -Wall -std=c++17 -g

# define linker flags for the library
LDFLAGS=-shared -lcppsdl -lm

# define linker flags for the test program
TFLAGS=-lm -lcppsdl -L./lib -Wl,-rpath,./lib -lorbeng

# retrieve source code for the library
LENT=$(shell ls src/lib/entity/*.cpp)
LTIL=$(shell ls src/lib/tile/*.cpp)
LDIE=$(shell ls src/lib/dice/*.cpp)
LEXC=$(shell ls src/lib/except/*.cpp)
LUTI=$(shell ls src/lib/util/*.cpp)
LWLD=$(shell ls src/lib/world/*.cpp)

# list the source code for the library
LSOURCES=$(LENT) $(LTIL) $(LDIE) $(LEXC) $(LUTI) $(LWLD)

# compile the source code for the library
LOBJECTS=$(LSOURCES:.cpp=.o)

# retrieve source code for the test program
TMAIN=$(shell ls src/test/*.cpp)

# list the source code for the test program
TSOURCES=$(TMAIN)

# compile the source code for the test program
TOBJECTS=$(TSOURCES:.cpp=.o)

# define the name of the library
LIB=liborbeng.so

# define the name of the test executable
TEST=demo

# rule for building both the library and the test program
all: library test

# master rule for compiling the library
library: $(LSOURCES) $(LIB)

# master rule for compiling the test program
test: $(TSOURCES) $(TEST)

# sub-rule for compiling the library
$(LIB): $(LOBJECTS)
	$(CXX) $(LOBJECTS) -o $@ $(LDFLAGS)
	mkdir lib
	mkdir lobj
	mv -f $(LOBJECTS) lobj/
	mv -f $@ lib/

# sub-rule for compiling the test program
$(TEST): $(TOBJECTS)
	$(CXX) $(TOBJECTS) -o $@ $(TFLAGS)
	mkdir bin
	mkdir tobj
	mv -f $(TOBJECTS) tobj/
	mv -f $@ bin/

# rule for compiling source code to object code
.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

# target to install the compiled library
# REQUIRES ROOT
install:
	if [ -f "/usr/lib/liborbeng.so" ]; then \
		rm /usr/lib/liborbeng.so; \
	fi 
	if [ -d "/usr/include/orbeng" ]; then \
		rm -rf /usr/include/orbeng; \
	fi 
	
	mkdir /usr/include/orbeng
	mkdir /usr/include/orbeng/entity
	mkdir /usr/include/orbeng/tile
	mkdir /usr/include/orbeng/dice 
	mkdir /usr/include/orbeng/except 
	mkdir /usr/include/orbeng/util 
	mkdir /usr/include/orbeng/world 
	cp src/lib/orbeng.h /usr/include/orbeng/
	cp $(shell ls src/lib/entity/*.h) /usr/include/orbeng/entity/
	cp $(shell ls src/lib/tile/*.h) /usr/include/orbeng/tile/
	cp $(shell ls src/lib/dice/*.h) /usr/include/orbeng/dice/
	cp $(shell ls src/lib/except/*.h) /usr/include/orbeng/except/
	cp $(shell ls src/lib/util/*.h) /usr/include/orbeng/util/
	cp $(shell ls src/lib/world/*.h) /usr/include/orbeng/world/
	cp ./lib/$(LIB) /usr/lib/

# target to clean the workspace
clean:
	if [ -d "lobj" ]; then \
		rm -rf lobj; \
	fi
	if [ -d "lib" ]; then \
		rm -rf lib; \
	fi
	if [ -d "bin" ]; then \
		rm -rf bin; \
	fi
	if [ -d "tobj" ]; then \
		rm -rf tobj; \
	fi

# end of Makefile
