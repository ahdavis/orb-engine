/*
 * main.cpp
 * Test program for the Orb Engine
 * Created by Andrew Davis
 * Created on 6/18/18
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//includes
#include <iostream>
#include <cstdlib>
#include <cppsdl/cppsdl.h>
#include <vector>
#include <utility>
#include <string>
#include <map>
#include "../lib/orbeng.h"

//constants
const int WIN_WIDTH = 640; //the width of the window
const int WIN_HEIGHT = 480; //the height of the window
const int LEV_WIDTH = 1280; //the width of the level
const int LEV_HEIGHT = 960; //the height of the level
const int TILE_WIDTH = 80; //the width of each tile
const int TILE_HEIGHT = 80; //the height of each tile
const int TOTAL_TILES = 192; //the total number of tiles
const int TOTAL_SPRS = 12; //the total number of sprites
const int DOT_VEL = 10; //the maximum axis velocity of the dot

//these pairs contain the coordinates of the tiles' sprites
const OrbE::TSPair TILE_RED = {0, 0};
const OrbE::TSPair TILE_GREEN = {0, 1};
const OrbE::TSPair TILE_BLUE = {0, 2};
const OrbE::TSPair TILE_TOPLEFT = {1, 0};
const OrbE::TSPair TILE_LEFT = {1, 1};
const OrbE::TSPair TILE_BLEFT = {1, 2};
const OrbE::TSPair TILE_TOP = {2, 0};
const OrbE::TSPair TILE_CENTER = {2, 1};
const OrbE::TSPair TILE_BOTTOM = {2, 2};
const OrbE::TSPair TILE_TOPRIGHT = {3, 0};
const OrbE::TSPair TILE_RIGHT = {3, 1};
const OrbE::TSPair TILE_BRIGHT = {3, 2};

//declare a TileMapper to generate tiles from
OrbE::TileMapper tileMapper = {
		{0, TILE_RED},
		{1, TILE_GREEN},
		{2, TILE_BLUE},
		{3, TILE_CENTER},
		{4, TILE_TOP},
		{5, TILE_TOPRIGHT},
		{6, TILE_RIGHT},
		{7, TILE_BRIGHT},
		{8, TILE_BOTTOM},
		{9, TILE_BLEFT},
		{10, TILE_LEFT},
		{11, TILE_TOPLEFT}};

//global variables
CPPSDL::Rect dotHB(0, 0, 20, 20);
CPPSDL::Rect camera(0, 0, WIN_WIDTH, WIN_HEIGHT);
int dVelX = 0; //the x-velocity of the dot
int dVelY = 0; //the y-velocity of the dot

//function prototypes

//moves the dot
void moveDot(const std::vector<OrbE::Tile>& walls);

//centers the camera on the dot
void centerCamera();

//returns whether the dot is touching the wall
bool touchesWall(const std::vector<OrbE::Tile>& walls);

//main function - main entry point for the program
int main(int argc, char* argv[]) {
	//initialize CPPSDL
	CPPSDL::Init();

	//create a window
	CPPSDL::Window win("Tiling Demo", WIN_WIDTH, WIN_HEIGHT, true);

	//create a renderer
	CPPSDL::Renderer rend(win);

	//create a Bundle
	CPPSDL::Bundle bundle;

	//get the path to the bundle file
	std::string bundlePath = CPPSDL::GetBasePath();
	bundlePath += "../src/test/assets/demo.bundle";

	//load the bundle
	bundle.deserialize(bundlePath);

	//create an image element from the tiles image in the bundle
	std::string tilePath = bundle.assetForName("tiles.png").getPath();
	CPPSDL::ImgElement tileImg(tilePath);

	//create a texture from the image element
	CPPSDL::Texture tileTex(rend, tileImg);

	//get a spritesheet from the texture
	CPPSDL::SpriteSheet sheet(tileTex, TILE_WIDTH, TILE_HEIGHT);

	//create an image element from the dot file
	std::string dotPath = bundle.assetForName("dot.bmp").getPath();
	CPPSDL::ImgElement dotImg(dotPath);

	//color key the dot image element
	dotImg.setColorKey(CPPSDL::Color(0x00, 0xFF, 0xFF), true);

	//create a texture from the dot image
	CPPSDL::Texture dotTex(rend, dotImg);

	//create an event handler
	CPPSDL::EventHandler eh;

	//create a tile mapping functor
	OrbE::TileMapping mapper(tileMapper, TILE_WIDTH, TILE_HEIGHT,
					LEV_WIDTH, LEV_HEIGHT);

	//get a vector of Tiles from a file
	std::string mapPath = bundle.assetForName("lazy.map").getPath();
	OrbE::TileMap map(mapPath, mapper, TOTAL_TILES);
	std::vector<OrbE::Tile> tiles = map.getTiles();

	//create a string to hold the die equation
	std::string dieEqn = "2d6 + 5";

	//evaluate the die equation
	int dieRes = OrbE::evalDieEqn(dieEqn);

	//print the result
	std::cout << "Evaluated " << dieEqn << " and rolled a " 
		  << dieRes << "!" << std::endl;

	//create a sentinel variable
	bool quit = false;

	//game loop
	while(!quit) {
		//process the events
		while(eh.pollEvent() != 0) {
			CPPSDL::Event e = eh.getEvent(); //get each event
			auto type = e.getType(); //get the event type

			//process the event
			if(type == CPPSDL::Events::Quit) { //quit event
				quit = true;
			} else if(type == CPPSDL::Events::KeyDown
					&& e.getKeyRepeat() == 0) {
				auto key = e.getKey(); //get the key

				//process the key
				if(key == CPPSDL::Keys::up) {
					dVelY -= DOT_VEL;
				} else if(key == CPPSDL::Keys::down) {
					dVelY += DOT_VEL;
				} else if(key == CPPSDL::Keys::left) {
					dVelX -= DOT_VEL;
				} else if(key == CPPSDL::Keys::right) {
					dVelX += DOT_VEL;
				}
			} else if(type == CPPSDL::Events::KeyUp
					&& e.getKeyRepeat() == 0) {
				auto key = e.getKey(); //get the key

				//process the key
				if(key == CPPSDL::Keys::up) {
					dVelY += DOT_VEL;
				} else if(key == CPPSDL::Keys::down) {
					dVelY -= DOT_VEL;
				} else if(key == CPPSDL::Keys::left) {
					dVelX += DOT_VEL;
				} else if(key == CPPSDL::Keys::right) {
					dVelX -= DOT_VEL;
				}
			} 

		}

		//move the dot
		moveDot(tiles);

		//center the camera
		centerCamera();

		//clear the renderer
		rend.setDrawColor(CPPSDL::Color());
		rend.clear();

		//render the tiles
		for(const OrbE::Tile& t : tiles) {
			if(camera.collidesWith(t.getBounds())) {
				rend.renderTexture(sheet, t.getSprX(), 
						t.getSprY(),
						t.getBounds().getX() -
						camera.getX(),
						t.getBounds().getY() -
						camera.getY());

			}

		}

		//render the dot
		rend.renderTexture(dotTex, dotHB.getX() - camera.getX(),
					dotHB.getY() - camera.getY());

		//update the renderer
		rend.update();

	}

	//write the bundle object back into the file
	bundle.serialize(bundlePath);

	//and return with no errors
	return EXIT_SUCCESS;
}

	
//moveDot function - moves the dot
void moveDot(const std::vector<OrbE::Tile>& walls) {
	//move the dot in the X direction
	dotHB.setX(dotHB.getX() + dVelX);

	//handle the dot being out of range
	if((dotHB.getX() < 0) || (dotHB.getX() + 20 >
						LEV_WIDTH) ||
			touchesWall(walls)) {
		dotHB.setX(dotHB.getX() - dVelX);
	}
	
	//move the dot in the Y direction
	dotHB.setY(dotHB.getY() + dVelY);

	//handle the dot being out of range
	if((dotHB.getY() < 0) || (dotHB.getY() + 20 >
						LEV_HEIGHT) ||
			touchesWall(walls)) {
		dotHB.setY(dotHB.getY() - dVelY);
	}


}

//centerCamera function - centers the camera over the dot
void centerCamera() {
	//center the camera over the dot
	camera.setX((dotHB.getX() + dotHB.getWidth()) - WIN_WIDTH / 2);
	camera.setY((dotHB.getY() + dotHB.getHeight()) - WIN_HEIGHT / 2);

	//keep the camera in bounds
	if(camera.getX() < 0) {
		camera.setX(0);
	} 

	if(camera.getY() < 0) {
		camera.setY(0);
	}

	if(camera.getX() > (LEV_WIDTH - camera.getWidth())) {
		camera.setX(LEV_WIDTH - camera.getWidth());
	}

	if(camera.getY() > (LEV_HEIGHT - camera.getHeight())) {
		camera.setY(LEV_HEIGHT - camera.getHeight());
	}
}

//touchesWall function - returns whether the dot is touching a wall
bool touchesWall(const std::vector<OrbE::Tile>& walls) {
	//loop through the tiles
	for(const OrbE::Tile& t : walls) {
		if((t.getID() >= 3) && (t.getID() <= 11)) {
			if(dotHB.collidesWith(t.getBounds())) {
				return true; //collision found
			}
		}
	}

	//no collision, so return false
	return false;
}

//end of program
