/*
 * Chunk.h
 * Declares a class that represents a world chunk
 * Created by Andrew Davis
 * Created on 6/25/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <cppsdl/cppsdl.h>
#include <string>
#include <vector>
#include <iostream>
#include "../tile/TileMap.h"
#include "../entity/Entity.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class Chunk final {
		//public fields and methods
		public:
			//default constructor
			Chunk();

			//first main constructor
			Chunk(const TileMap& newMap, int newX, int newY,
					int newWidth, int newHeight,
				const std::vector<Entity>& newEntities,
					int newID);

			//second main constructor
			Chunk(const TileMap& newMap, 
					const CPPSDL::Rect& newBounds,
				const std::vector<Entity>& newEntities,
					int newID);

			//destructor
			~Chunk();

			//copy constructor
			Chunk(const Chunk& c);

			//assignment operator
			Chunk& operator=(const Chunk& src);

			//getter methods
			
			//returns the TileMap for the Chunk
			const TileMap& getTileMap() const;

			//returns the path to the Chunk's tilemap
			const std::string& getTileMapPath() const;

			//returns the bounds of the Chunk
			const CPPSDL::Rect& getBounds() const;

			//returns the Entities residing within the Chunk
			const std::vector<Entity>& getEntities() const;

			//returns the ID of the Chunk
			int getID() const;

			//setter methods
			
			//sets the TileMap for the the Chunk
			void setTileMap(const TileMap& newMap);

			//sets the Entities residing within the Chunk
			void setEntities(const std::vector<Entity>& 
						newEntities);

			//other methods
			
			//serialization operator
			friend std::ostream& operator<<(std::ostream& os,
							const Chunk& c);

			//deserialization operator
			friend std::istream& operator>>(std::istream& is,
							Chunk& c);

			//adds an Entity to the Chunk
			void addEntity(const Entity& e);

			//removes an Entity from the Chunk by its ID
			//returns whether the Entity was removed
			//successfully
			bool rmEntityByID(int id);

			//removes an Entity from the Chunk by its name
			//returns whether the Entity was removed
			//successfully
			bool rmEntityByName(const std::string& name);

		//private fields and methods
		private:
			//friendship declaration
			friend class World;

			//fields
			TileMap tmap; //the tilemap for the Chunk
			CPPSDL::Rect bounds; //the bounds of the Chunk
			std::vector<Entity> entities; //the chunk entities
			int id; //the chunk's ID
	};
}

//end of header
