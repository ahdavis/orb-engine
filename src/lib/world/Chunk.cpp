/*
 * Chunk.cpp
 * Implements a class that represents a world chunk
 * Created by Andrew Davis
 * Created on 6/25/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Chunk.h"

//includes
#include <fstream>
#include <sstream>
#include <cstdlib>

//default constructor
OrbE::Chunk::Chunk()
	: OrbE::Chunk(OrbE::TileMap("", 0), 0, 0, 1, 1, 
			std::vector<OrbE::Entity>(), 0)
{
	//no code needed
}

//first constructor
OrbE::Chunk::Chunk(const OrbE::TileMap& newMap, int newX, int newY,
			int newWidth, int newHeight,
			const std::vector<OrbE::Entity>& newEntities,
			int newID)
	: OrbE::Chunk(newMap, CPPSDL::Rect(newX, newY, newWidth, 
				newHeight), newEntities, newID)
{
	//no code needed
}

//second constructor
OrbE::Chunk::Chunk(const OrbE::TileMap& newMap, const CPPSDL::Rect&
			newBounds, 
			const std::vector<OrbE::Entity>& newEntities,
			int newID)
	: tmap(newMap), bounds(newBounds), entities(newEntities), 
		id(newID)
{
	//no code needed
}

//destructor
OrbE::Chunk::~Chunk() {
	//no code needed
}

//copy constructor
OrbE::Chunk::Chunk(const OrbE::Chunk& c)
	: tmap(c.tmap), bounds(c.bounds), entities(c.entities), id(c.id)
{
	//no code needed
}

//assignment operator
OrbE::Chunk& OrbE::Chunk::operator=(const OrbE::Chunk& src) {
	this->tmap = src.tmap; //assign the tilemap
	this->bounds = src.bounds; //assign the bounds
	this->entities = src.entities; //assign the entity vector
	this->id = src.id; //assign the ID field
	return *this; //and return the instance
}

//getTileMap method - returns the Chunk's tilemap
const OrbE::TileMap& OrbE::Chunk::getTileMap() const {
	return this->tmap; //return the tilemap field
}

//getTileMapPath method - returns the path to the Chunk's tilemap
const std::string& OrbE::Chunk::getTileMapPath() const {
	return this->tmap.getSourcePath();
}

//getBounds method - returns the bounds of the Chunk
const CPPSDL::Rect& OrbE::Chunk::getBounds() const {
	return this->bounds; //return the bounding rect
}

//getEntities method - returns the entities residing within the Chunk
const std::vector<OrbE::Entity>& OrbE::Chunk::getEntities() const {
	return this->entities; //return the entity vector
}

//getID method - returns the Chunk's ID
int OrbE::Chunk::getID() const {
	return this->id; //return the ID field
}

//setTilemap method - sets the tilemap for the Chunk
void OrbE::Chunk::setTileMap(const OrbE::TileMap& newMap) {
	this->tmap = newMap; //assign the tilemap
}

//setEntities method - sets the entities within the Chunk
void OrbE::Chunk::setEntities(const std::vector<OrbE::Entity>& 
				newEntities) {
	this->entities = newEntities; //assign the entity vector
}

//addEntity method - adds an entity to the Chunk
void OrbE::Chunk::addEntity(const OrbE::Entity& e) {
	this->entities.push_back(e); //add the entity to the vector
}

//rmEntityByID method - removes an entity from the Chunk by its ID
bool OrbE::Chunk::rmEntityByID(int id) {
	//loop and remove the entity
	for(std::size_t i = 0; i < this->entities.size(); i++) {
		if(this->entities[i].getID() == id) { //id match
			//remove the entity
			this->entities.erase(this->entities.begin() + i);

			//and return a success
			return true;
		}
	}

	//no match, so return false
	return false;
}

//rmEntityByName method - removes an entity from the Chunk by its name
bool OrbE::Chunk::rmEntityByName(const std::string& name) {
	//loop and remove the entity
	for(std::size_t i = 0; i < this->entities.size(); i++) {
		if(this->entities[i].getName() == name) { //name match
			//remove the entity
			this->entities.erase(this->entities.begin() + i);

			//and return a success
			return true;
		}
	}

	//no match, so return false
	return false;
}

namespace OrbE {
	//serialization operator
	std::ostream& operator<<(std::ostream& os, const Chunk& c) {
		//serialize the ID
		os << c.id << '\n';

		//serialize the bounding rectangle
		os << c.bounds.getX() << ' ';
		os << c.bounds.getY() << ' ';
		os << c.bounds.getWidth() << ' ';
		os << c.bounds.getHeight() << '\n';

		//serialize the entity count
		os << c.entities.size() << '\n';
		
		//loop and serialize each entity
		for(const auto& e : c.entities) {
			os << e << '\n';
		}

		//and return the stream
		return os;
	}

	//deserialization operator
	std::istream& operator>>(std::istream& is, Chunk& c) {
		//deserialize the ID
		is >> c.id;

		//deserialize the bounding rectangle
		int x, y, w, h;
		is >> x;
		is >> y;
		is >> w;
		is >> h;
		c.bounds = CPPSDL::Rect(x, y, w, h);

		//clear the current entity vector
		c.entities.clear();

		//load the entity count
		int count = 0;
		is >> count;

		//loop and load each entity
		for(int i = 0; i < count; i++) {
			Entity e;
			is >> e;
			c.entities.push_back(e);
		}

		//and return the stream
		return is;
	}
}

//end of implementation
