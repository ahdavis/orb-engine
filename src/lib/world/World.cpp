/*
 * World.cpp
 * Implements a class that represents a game world
 * Created by Andrew Davis
 * Created on 6/28/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "World.h"

//includes
#include "../tile/TileMap.h"
#include <cmath>
#include <fstream>
#include <cstdlib>

//constant initialization
const int OrbE::World::DEFAULT_MAX = 25;
const int OrbE::World::DEFAULT_PLAY_X = 0;
const int OrbE::World::DEFAULT_PLAY_Y = 0;

//default constructor
OrbE::World::World()
	: OrbE::World(DEFAULT_MAX) //call the first main constructor
{
	//no code needed
}

//first main constructor
OrbE::World::World(int newMaxChunks)
	: OrbE::World(newMaxChunks, DEFAULT_PLAY_X, DEFAULT_PLAY_Y)
{
	//no code needed
}

//second main constructor
OrbE::World::World(int newMaxChunks, int newPlayX, int newPlayY)
	: loadedChunks(), maxChunks(1), worldChunks(),
		playX(newPlayX), playY(newPlayY), loadRange(0)
{
	this->setMaxChunks(newMaxChunks); //set the maximum chunk count
	this->updateLoadRange(); //calculate the load range
}

//destructor
OrbE::World::~World() {
	this->unloadAll(); //unload all the loaded chunks
}

//copy constructor
OrbE::World::World(const OrbE::World& w)
	: loadedChunks(w.loadedChunks), maxChunks(w.maxChunks),
		worldChunks(w.worldChunks), playX(w.playX), playY(w.playY),
			loadRange(w.loadRange)
{
	//no code needed
}

//assignment operator
OrbE::World& OrbE::World::operator=(const OrbE::World& src) {
	this->unloadAll(); //unload the currently loaded chunks
	this->loadedChunks = src.loadedChunks; //assign the loaded chunks
	this->maxChunks = src.maxChunks; //assign the max chunk count
	this->worldChunks = src.worldChunks; //assign the world chunks
	this->playX = src.playX; //assign the player's x-coord
	this->playY = src.playY; //assign the player's y-coord
	this->loadRange = src.loadRange; //assign the load range
	return *this; //and return the instance
}

//getLoadedChunks method - returns a vector containing
//the currently loaded chunks
std::vector<OrbE::Chunk> OrbE::World::getLoadedChunks() const {
	std::vector<OrbE::Chunk> ret; //the return value

	//loop and put the loaded chunks into the return vector
	for(auto it = this->loadedChunks.begin(); 
			it != this->loadedChunks.end(); ++it) {
		ret.push_back(*it);
	}

	//and return the populated vector
	return ret;
}

//getMaxChunks method - returns the maximum number of loaded chunks
int OrbE::World::getMaxChunks() const {
	return this->maxChunks; //return the maximum chunk count field
}

//getChunkCount method - returns the current number of loaded chunks
int OrbE::World::getChunkCount() const {
	return this->loadedChunks.size(); //return the chunk count
}

//chunkIsLoaded method - returns whether a chunk is currently loaded
bool OrbE::World::chunkIsLoaded(int chunkID) const {
	//loop and check each loaded chunk for an ID match
	for(auto it = this->loadedChunks.begin();
			it != this->loadedChunks.end(); ++it) {
		if(it->getID() == chunkID) { //if the chunk ID matches
			return true; //then return true
		}
	}

	//no match, so return false
	return false;
}

//setMaxChunks method - sets the maximum number of loaded chunks
void OrbE::World::setMaxChunks(int newMaxChunks) {
	if(newMaxChunks < 1) { //if the new chunk count is invalid
		newMaxChunks = 1; //then make it 1
	}

	this->maxChunks = newMaxChunks; //set the chunk count
	this->updateLoadRange(); //and update the load range
}

//updatePlayerCoords method - updates the coordinates of the player
void OrbE::World::updatePlayerCoords(int newX, int newY) {
	this->playX = newX; //update the x-coord
	this->playY = newY; //update the y-coord
}

//first addChunk method - adds a Chunk to the World without loading it
void OrbE::World::addChunk(const OrbE::Chunk& c) {
	this->addChunk(c, false); //call the other addChunk method
}

//second addChunk method - adds a Chunk to the World
void OrbE::World::addChunk(const OrbE::Chunk& c, bool shouldLoad) {
	this->worldChunks.push_back(c); //add the chunk to the world

	//handle whether the chunk should load right away
	if(shouldLoad) { //if the chunk should be loaded
		this->loadChunk(c); //then load it
	}
}

//unloadAll method - unloads all Chunks from the World
void OrbE::World::unloadAll() {
	this->loadedChunks.clear(); //clear the loaded chunks
}

//unloadEarliest method - unloads the earliest loaded Chunk
void OrbE::World::unloadEarliest() {
	this->loadedChunks.pop_front(); //unload the earliest Chunk
}

//loadChunk method - loads a Chunk into the World
bool OrbE::World::loadChunk(const OrbE::Chunk& c) {
	//make sure that the maximum number of 
	//loaded chunks is not exceeded
	if(this->loadedChunks.size() >= this->maxChunks) {
		//unload the earliest chunk
		this->unloadEarliest();

		//load the argument chunk
		this->loadedChunks.push_back(c);

		//and return true
		return true;
	} else { //maximum number not reached
		this->loadedChunks.push_back(c); //load the chunk

		return false; //and return false
	}
}

//loadChunks method - loads all chunks within range of the player
void OrbE::World::loadChunks() {
	//get the chunks within range of the player
	std::vector<OrbE::Chunk> chunks = this->getRangeChunks();

	//and load all the chunks that are not currently loaded
	for(const auto& c : chunks) {
		if(!this->chunkIsLoaded(c.getID())) {
			this->loadChunk(c);	
		}
	}
}

//overridden serialize method - serializes the World
bool OrbE::World::serialize(const std::string& path) {
	//open the save file
	std::ofstream ofs;
	ofs.open(path);

	//serialize the player's coordinates
	ofs << this->playX << ' ';
	ofs << this->playY << '\n';

	//serialize the number of world chunks
	ofs << this->worldChunks.size() << '\n';

	//serialize each world chunk
	for(const auto& wc : this->worldChunks) {
		ofs << wc << '\n';
	}

	//serialize each tilemap
	for(auto& c : this->worldChunks) {
		//serialize the data for each tilemap
		ofs << c.tmap.getSourcePath() << '\n';
		ofs << c.tmap.getTileCount() << '\n';
		ofs << c.tmap.getMapping() << '\n';

		//serialize each tilemap
		c.tmap.serialize(c.tmap.getSourcePath());	
	}

	//serialize the maximum number of chunks
	ofs << this->maxChunks << '\n';

	//close the file
	ofs.close();

	//and return a success
	return true;
}

//overridden deserialize method - deserializes a World
bool OrbE::World::deserialize(const std::string& path) {
	//make sure that the source file exists
	if(!CPPSDL::FileExists(path)) { //if the file does not exist
		return false; //then return a failure
	}

	//clear the current world chunks
	this->worldChunks.clear();

	//open the file to deserialize from
	std::ifstream ifs;
	ifs.open(path);

	//deserialize the player's coordinates
	ifs >> this->playX;
	ifs >> this->playY;

	//deserialize the number of world chunks
	int wcCt = 0;
	ifs >> wcCt;

	//loop and deserialize each chunk
	for(int i = 0; i < wcCt; i++) {
		OrbE::Chunk ch;
		ifs >> ch;
		this->worldChunks.push_back(ch);
	}

	//deserialize each tilemap
	for(int i = 0; i < wcCt; i++) {
		//declare variables to read in to
		std::string sPath;
		int ct;
		OrbE::TileMapping tm;

		//load the values of those variables
		std::getline(ifs, sPath); //load the path string
		ifs >> ct; //load the tile count
		ifs >> tm; //load the tile mapping functor

		//create a TileMap object from the variables
		//but not load its tiles
		OrbE::TileMap map(sPath, ct);

		//update its mapping functor
		map.setMapping(tm);

		//deserialize the tilemap
		map.deserialize(map.getSourcePath());

		//and put it into the world chunk vector
		this->worldChunks[i].tmap = map;
	}

	//deserialize the maximum number of loaded chunks
	ifs >> this->maxChunks;

	//load the in-range chunks
	this->updateLoadRange();
	this->loadChunks();

	//and return a success
	return true;

}

//protected updateLoadRange method - updates the load range
void OrbE::World::updateLoadRange() {
	//get the magnitude of the load range vector
	//using the Pythagorean Theorem
	//the vector has an origin of (0, 0)
	double w = std::sqrt(this->maxChunks);
	double m = std::sqrt(2 * std::pow(w / 2, 2));

	//and cast the magnitude to an int and 
	//assign it to the load range field
	this->loadRange = static_cast<int>(m);
}

//protected getRangeChunks method - returns a vector containing the
//chunks within the load range
std::vector<OrbE::Chunk> OrbE::World::getRangeChunks() const {
	std::vector<OrbE::Chunk> ret; //the return value
	
	//loop through a circle around the player
	for(double t = 0.0; t < (2.0 * M_PI); t += (M_PI / 180.0)) {
		//get the current circular x-coord and y-coord
		int x = this->loadRange * std::cos(t);
		int y = this->loadRange * std::sin(t);

		//add the coordinates to the current player coordinates
		x += this->playX;
		y += this->playY;

		//loop and find chunks that are within the coordinates
		for(const auto& c : this->worldChunks) {
			//get the coordinates of each chunk
			int cx = c.getBounds().getX();
			int cy = c.getBounds().getY();

			//determine whether the coordinates are within
			//the circular coordinates
			if((x >= cx) && (y >= cy)) {
				//they are, so add the chunk to the
				//return vector
				ret.push_back(c);
			}
		}
	}

	//and return the return value
	return ret;	
}

//end of implementation
