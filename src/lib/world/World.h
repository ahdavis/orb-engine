/*
 * World.h
 * Declares a class that represents a game world
 * Created by Andrew Davis
 * Created on 6/28/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <deque>
#include <vector>
#include <cstdlib>
#include "Chunk.h"
#include <cppsdl/cppsdl.h>

//namespace declaration
namespace OrbE {
	//class declaration
	class World : public CPPSDL::Serializable
	{
		//public fields and methods
		public:
			//default constructor
			World();

			//first main constructor
			explicit World(int newMaxChunks);

			//second main constructor
			World(int newMaxChunks, int newPlayX, 
				int newPlayY);

			//destructor
			virtual ~World();

			//copy constructor
			World(const World& w);

			//assignment operator
			World& operator=(const World& src);

			//getter methods
			
			//returns a vector containing all loaded chunks
			std::vector<Chunk> getLoadedChunks() const;

			//returns the maximum number of loaded chunks
			int getMaxChunks() const;

			//returns the current number of loaded chunks
			int getChunkCount() const;

			//returns whether a given chunk is loaded
			bool chunkIsLoaded(int chunkID) const;

			//setter methods
			
			//sets the maximum number of loaded chunks
			void setMaxChunks(int newMaxChunks);

			//updates the coordinates of the player
			void updatePlayerCoords(int newX, int newY);

			//other methods
			
			//adds a chunk to the world without loading it
			void addChunk(const Chunk& c);

			//adds a chunk to the world
			void addChunk(const Chunk& c, bool shouldLoad);
			
			//unloads all the chunks in the world
			void unloadAll();

			//unloads the earliest loaded chunk in the world
			void unloadEarliest();

			//loads a chunk into the world
			//if the maximum number of chunks is loaded,
			//then the earliest chunk will be unloaded.
			//returns whether a chunk was unloaded
			bool loadChunk(const Chunk& c);

			//loads the chunks within the load range
			//relative to the player
			void loadChunks();

			//serializes the World
			bool serialize(const std::string& path) override;

			//deserializes the World
			bool deserialize(const std::string& path) override;

			//generates the World
			virtual void generate() = 0;

		//protected fields and methods
		protected:
			//methods
			void updateLoadRange(); //updates the load range

			//returns a vector of chunks within the load range
			//relative to the player
			std::vector<Chunk> getRangeChunks() const;

			//constants
			
			//the default maximum number of loaded chunks
			static const int DEFAULT_MAX;

			//the default x-coord for the player's start
			static const int DEFAULT_PLAY_X;

			//the default y-coord for the player's start
			static const int DEFAULT_PLAY_Y;

			//fields
			std::deque<Chunk> loadedChunks; //the loaded chunks
			std::size_t maxChunks; //the maximum chunk count
			std::vector<Chunk> worldChunks; //the world chunks
			int playX; //the x-coord of the player
			int playY; //the y-coord of the player
			int loadRange; //the range for chunks to load
	};
}

//end of header
