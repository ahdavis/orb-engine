/*
 * functions.h
 * Declares utility functions for the Orb Engine
 * Created by Andrew Davis
 * Created on 6/21/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include <string>

//namespace declaration
namespace OrbE {
	//function declarations
	
	//returns whether a character is a digit
	bool isDigit(char c);

	//returns whether a character is a letter
	bool isAlpha(char c);

	//returns whether a character is a space
	bool isSpace(char c);

	//returns whether a file exists at a path
	bool fileExists(const std::string& path);
}

//end of header
