/*
 * functions.cpp
 * Implements utility functions for the Orb Engine
 * Created by Andrew Davis
 * Created on 6/21/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "functions.h"

//include
#include <cppsdl/cppsdl.h>

//isDigit function - returns whether a character is a digit
bool OrbE::isDigit(char c) {
	//return whether the character is a digit
	return ((c >= '0') && (c <= '9'));
}

//isAlpha function - returns whether a character is a letter
bool OrbE::isAlpha(char c) {
	//get the components of the comparison
	bool isLower = ((c >= 'a') && (c <= 'z'));
	bool isUpper = ((c >= 'A') && (c <= 'Z'));

	//and return the logical OR of the components
	return isLower || isUpper;
}

//isSpace function - returns whether a character is a space
bool OrbE::isSpace(char c) {
	return (c == 0x20); //0x20 is ASCII for a space
}

//fileExists function - returns whether a file exists at a path
bool OrbE::fileExists(const std::string& path) {
	return CPPSDL::FileExists(path); //call the CPPSDL fileExists fn
}

//end of implementation
