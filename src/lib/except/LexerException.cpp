/*
 * LexerException.cpp
 * Implements an exception that is thrown when the die roll lexer
 * encounters an error
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "LexerException.h"

//include
#include <sstream>

//constructor
OrbE::LexerException::LexerException(char badChar)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Error: unknown character ";
	ss << badChar;
	this->errMsg = ss.str();
}

//destructor
OrbE::LexerException::~LexerException() {
	//no code needed
}

//copy constructor
OrbE::LexerException::LexerException(const OrbE::LexerException& le)
	: errMsg(le.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
OrbE::LexerException& OrbE::LexerException::operator=(const
					OrbE::LexerException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* OrbE::LexerException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
