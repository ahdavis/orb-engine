/*
 * TileException.cpp
 * Implements an exception that is thrown when an invalid tile ID is found
 * Created by Andrew Davis
 * Created on 7/10/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "TileException.h"

//include
#include <sstream>

//constructor
OrbE::TileException::TileException(int badID)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Bad tile ID: ";
	ss << badID;
	this->errMsg = ss.str();
}

//destructor
OrbE::TileException::~TileException() {
	//no code needed
}

//copy constructor
OrbE::TileException::TileException(const OrbE::TileException& te)
	: errMsg(te.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
OrbE::TileException& OrbE::TileException::operator=(const 
						OrbE::TileException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* OrbE::TileException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
