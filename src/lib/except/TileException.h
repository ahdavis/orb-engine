/*
 * TileException.h
 * Declares an exception that is thrown when an invalid tile ID is found
 * Created by Andrew Davis
 * Created on 7/10/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>

//namespace declaration
namespace OrbE {
	//class declaration
	class TileException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit TileException(int badID);

			//destructor
			~TileException();

			//copy constructor
			TileException(const TileException& te);

			//assignment operator
			TileException& operator=(const TileException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
