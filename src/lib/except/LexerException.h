/*
 * LexerException.h
 * Declares an exception that is thrown when the die roll lexer
 * encounters an error
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>

//namespace declaration
namespace OrbE {
	//class declaration
	class LexerException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit LexerException(char badChar);

			//destructor
			~LexerException();

			//copy constructor
			LexerException(const LexerException& le);

			//assignment operator
			LexerException& operator=(const LexerException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
