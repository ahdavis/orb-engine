/*
 * ParserException.cpp
 * Implements an exception that is thrown when the die roll parser
 * encounters an error
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "ParserException.h"

//constructor
OrbE::ParserException::ParserException()
	: errMsg("Error: syntax error") //init the error message
{
	//no code needed
}

//destructor
OrbE::ParserException::~ParserException() {
	//no code needed
}

//copy constructor
OrbE::ParserException::ParserException(const OrbE::ParserException& pe)
	: errMsg(pe.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
OrbE::ParserException& OrbE::ParserException::operator=(const
					OrbE::ParserException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* OrbE::ParserException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
