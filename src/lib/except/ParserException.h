/*
 * ParserException.h
 * Declares an exception that is thrown when the die roll parser
 * encounters an error
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>

//namespace declaration
namespace OrbE {
	//class declaration
	class ParserException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			ParserException();

			//destructor
			~ParserException();

			//copy constructor
			ParserException(const ParserException& pe);

			//assignment operator
			ParserException& operator=(const ParserException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
