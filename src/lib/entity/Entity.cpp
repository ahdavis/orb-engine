/*
 * Entity.cpp
 * Implements an abstract class that represents an Orb Engine entity
 * Created by Andrew Davis
 * Created on 6/18/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Entity.h"

//static field initialization
int OrbE::Entity::numCreated = 0;
int OrbE::Entity::curID = 0;

//default constructor
OrbE::Entity::Entity()
	: OrbE::Entity(CPPSDL::Rect(0, 0, 1, 1), 0, "")
{
	//no code needed
}

//constructor
OrbE::Entity::Entity(const CPPSDL::Rect& newHitbox, int newType,
			const std::string& newName)
	: hitbox(newHitbox), id(++curID), 
		coords(newHitbox.getX(), newHitbox.getY()), type(newType),
			name(newName)
{
	//increment the number of entities in existence
	numCreated++;
}

//destructor
OrbE::Entity::~Entity() {
	numCreated--; //decrement the creation count
}

//copy constructor
OrbE::Entity::Entity(const OrbE::Entity& e)
	: hitbox(e.hitbox), id(++curID), 
		coords(e.coords), type(e.type), name(e.name)
{
	//increment the number of entities in existence
	numCreated++;
}

//assignment operator
OrbE::Entity& OrbE::Entity::operator=(const OrbE::Entity& src) {
	this->hitbox = src.hitbox; //assign the hitbox
	//do not assign the ID
	this->coords = src.coords; //assign the coordinates
	this->type = src.type; //assign the type
	this->name = src.name; //assign the name
	return *this; //and return the instance
}

//static getCurrentEntityCount method - returns the current number of
//entities in existence
int OrbE::Entity::getCurrentEntityCount() {
	return numCreated; //return the creation count
}

//getID method - returns the entity's ID
int OrbE::Entity::getID() const {
	return this->id; //return the ID field
}

//getCoords method - returns the entity's coordinates
const CPPSDL::Point& OrbE::Entity::getCoords() const {
	return this->coords; //return the coords field
}	

//getType method - returns the entity's type
int OrbE::Entity::getType() const {
	return this->type; //return the type field
}

//getName method - returns the entity's name
const std::string& OrbE::Entity::getName() const {
	return this->name; //return the name field
}

//setCoords method - sets the entity's coordinates
void OrbE::Entity::setCoords(const CPPSDL::Point& newCoords) {
	this->coords = newCoords; //update the coordinates
	this->hitbox.setX(newCoords.getX()); //update the hitbox' x-coord
	this->hitbox.setY(newCoords.getY()); //update the hitbox' y-coord
}

//setName method - sets the entity's name
void OrbE::Entity::setName(const std::string& newName) {
	this->name = newName; //set the name field
}

//collidesWith method - returns whether the entity is in collision with
//another entity
bool OrbE::Entity::collidesWith(const OrbE::Entity& other) const {
	//return whether the hitboxes collide
	return this->hitbox.collidesWith(other.hitbox);
}

//virtual interactWith method - interacts with another entity
void OrbE::Entity::interactWith(const OrbE::Entity& other) {
	//log that the base class version of this method was called
	CPPSDL::LogWarning("Entity::interactWith called on base class");
}

namespace OrbE {
	//serialization operator
	std::ostream& operator<<(std::ostream& os, const Entity& e) {
		//serialize the hitbox
		os << e.hitbox.getX() << ' ';
		os << e.hitbox.getY() << ' ';
		os << e.hitbox.getWidth() << ' ';
		os << e.hitbox.getHeight() << ' ';

		//serialize the coordinates
		os << e.coords.getX() << ' ';
		os << e.coords.getY() << ' ';

		//serialize the entity type
		os << e.type << ' ';

		//serialize the entity name
		os << e.name << '\n';

		//and return the stream
		return os;
	}

	//deserialization operator
	std::istream& operator>>(std::istream& is, Entity& e) {
		//deserialize the hitbox
		int hx, hy, w, h;
		is >> hx;
		is >> hy; 
		is >> w; 
		is >> h;
		e.hitbox = CPPSDL::Rect(hx, hy, w, h);

		//deserialize the coordinates
		int x, y;
		is >> x;
		is >> y;
		e.coords = CPPSDL::Point(x, y);

		//deserialize the entity type
		is >> e.type;

		//deserialize the entity name
		is >> e.name;

		//and return the stream
		return is;
	}
}

//end of implementation
