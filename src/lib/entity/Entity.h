/*
 * Entity.h
 * Declares an abstract class that represents an Orb Engine entity
 * Created by Andrew Davis
 * Created on 6/18/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <string>
#include <cppsdl/cppsdl.h>
#include <iostream>

//namespace declaration
namespace OrbE {
	//class declaration
	class Entity {
		//public fields and methods
		public:
			//default constructor
			Entity();

			//constructor
			Entity(const CPPSDL::Rect& newHitbox, int newType,
					const std::string& newName);

			//destructor
			virtual ~Entity();

			//copy constructor
			Entity(const Entity& e);

			//assignment operator
			Entity& operator=(const Entity& src);

			//getter methods
			
			//returns the current number of entities in
			//existence
			static int getCurrentEntityCount();

			//returns the ID of the entity
			int getID() const;

			//returns the coordinates of the entity
			const CPPSDL::Point& getCoords() const;

			//returns the type of the entity
			int getType() const;

			//returns the name of the entity
			const std::string& getName() const;

			//setter methods
			
			//sets the coordinates of the entity
			void setCoords(const CPPSDL::Point& newCoords);

			//sets the name of the entity
			void setName(const std::string& newName);

			//other methods
			
			//returns whether the entity is colliding with
			//another entity
			bool collidesWith(const Entity& other) const;

			//interacts with another entity
			virtual void interactWith(const Entity& other);

			//serialization operators
			
			//serialization operator
			friend std::ostream& operator<<(std::ostream& os,
							const Entity& e);

			//deserialization operator
			friend std::istream& operator>>(std::istream& is,
							Entity& e);

		//protected fields and methods
		protected:
			//fields
			static int numCreated; //the number of entities
			static int curID; //the current entity ID
			CPPSDL::Rect hitbox; //the entity's hitbox
			int id; //the entity's ID
			CPPSDL::Point coords; //the entity's coordinates
			int type; //the entity's type
			std::string name; //the entity's name
	};
}

//end of header
