/*
 * Movable.h
 * Declares an interface to be implemented by moving entities
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include <cppsdl/cppsdl.h>

//namespace declaration
namespace OrbE {
	//interface declaration
	class Movable {
		//public methods
		public:
			//methods
			
			//returns the current position of the Movable
			//instance as a CPPSDL Point
			virtual CPPSDL::Point getPosition() const = 0;

			//absolutely sets the current position of the
			//Movable instance
			virtual void setPosition(const CPPSDL::Point&) = 0;

			//offsets the x-coord of the Movable instance
			virtual void moveX(int) = 0;

			//offsets the y-coord of the Movable instance
			virtual void moveY(int) = 0;

		//protected methods
		protected:
			//default constructor
			Movable() = default;

			//copy constructor
			Movable(const Movable& m) = default;

			//assignment operator
			Movable& operator=(const Movable& src) = default;
	};
}

//end of interface
