/*
 * TSPair.h
 * Defines a typedef for tile sprite coordinate pairs
 * Created by Andrew Davis
 * Created on 7/10/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include <utility>

//namespace declaration
namespace OrbE {
	//typedef definition
	typedef std::pair<int, int> TSPair;
}

//end of header
