/*
 * TileMap.h
 * Declares a class that represents an arrangement of Tile objects
 * Created by Andrew Davis
 * Created on 6/19/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <vector>
#include <string>
#include "Tile.h"
#include "TileMapping.h"
#include <cppsdl/cppsdl.h>

//namespace declaration
namespace OrbE {
	//class declaration
	class TileMap final : public CPPSDL::Serializable 
	{
		//public fields and methods
		public:
			//constructor
			TileMap(const std::string& newSrcPath,
					const TileMapping& newMapping,
					int newTileCount);

			//destructor
			~TileMap();

			//copy constructor
			TileMap(const TileMap& tm);

			//assignment operator
			TileMap& operator=(const TileMap& src);

			//getter methods
			
			//returns the path to the TileMap's source file
			const std::string& getSourcePath() const;

			//returns a vector of Tile objects read from
			//the source file
			const std::vector<Tile>& getTiles() const;

			//returns the number of Tiles in the TileMap
			int getTileCount() const;

			//other methods
			
			//serializes the TileMap
			bool serialize(const std::string& path) override;

			//deserializes the TileMap
			bool deserialize(const std::string& path) override;

			//replaces a Tile in the TileMap and saves
			//the updated TileMap to its file
			//returns whether the tile was replaced
			//successfully
			bool replaceTile(const Tile& rt);

		//private fields and methods
		private:
			//friendship declarations
			friend class World;
			friend class Chunk;
			
			//deserialization constructor
			TileMap(const std::string& newSrcPath,
					int newTileCount);

			//methods
			const TileMapping& getMapping() const;
			void setMapping(const TileMapping& tm);

			//fields
			std::string srcPath; //the path to the source file
			std::vector<Tile> tiles; //the tilemap tiles
			TileMapping mapping; //maps IDs to tiles
			int tileCount; //the number of tiles in the map
	};
}

//end of header
