/*
 * TileMapping.h
 * Declares a functor class that maps integers to Tile objects
 * Created by Andrew Davis
 * Created on 7/10/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include "Tile.h"
#include "TSPair.h"
#include "TileMapper.h"
#include <map>
#include <vector>
#include <iostream>

//namespace declaration
namespace OrbE {
	//class declaration
	class TileMapping final {
		//public fields and methods
		public:
			//default constructor
			TileMapping();

			//first main constructor
			//constructs from a std::vector
			TileMapping(const std::vector<TSPair>& newData,
					int newTileWidth, 
						int newTileHeight,
						int newLevelWidth,
						int newLevelHeight);

			//second main constructor
			//constructs from a std::map
			TileMapping(const TileMapper& newData,
					int newTileWidth,
						int newTileHeight,
						int newLevelWidth,
						int newLevelHeight);

			//destructor
			~TileMapping();

			//copy constructor
			TileMapping(const TileMapping& tm);

			//assignment operator
			TileMapping& operator=(const TileMapping& src);

			//function call operator
			Tile operator()(int id);

			//getter method
			
			//returns the number of mappable Tiles
			int getTileCount() const;

			//other method
			
			//resets the mapping coordinates
			void resetMappingCoords();

			//serialization operator
			friend std::ostream& operator<<(std::ostream& os,
						const TileMapping& tm);

			//deserialization operator
			friend std::istream& operator>>(std::istream& is,
							TileMapping& tm);

		//private fields and methods
		private:
			//fields
			TileMapper db; //the Tile sprite coords
			int count; //the number of mappable Tiles
			int tileWidth; //the width of each Tile
			int tileHeight; //the height of each Tile
			int tileX; //the x-coord of the current mapped Tile
			int tileY; //the y-coord of the current mapped Tile
			int levWidth; //the width of the level
			int levHeight; //the height of the level
	};
}

//end of header
