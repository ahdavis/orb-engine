/*
 * Tile.h
 * Declares a class that represents a tile
 * Created by Andrew Davis
 * Created on 6/19/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <cppsdl/cppsdl.h>

//namespace declaration
namespace OrbE {
	//class declaration
	class Tile final {
		//public fields and methods
		public:
			//default constructor - zeroes all the fields
			Tile();

			//first constructor - constructs from the
			//components of a CPPSDL Rect
			//newSprX and newSprY are the indices of the Tile's
			//sprite on a CPPSDL SpriteSheet
			Tile(int x, int y, int w, int h,
				int newSprX, int newSprY, int newID);

			//second constructor - constructs from a 
			//CPPSDL Rect
			Tile(const CPPSDL::Rect& newBounds,
				int newSprX, int newSprY, int newID);

			//destructor
			~Tile();

			//copy constructor
			Tile(const Tile& t);

			//assignment operator
			Tile& operator=(const Tile& src);

			//getter methods
			
			//returns the bounds of the Tile
			const CPPSDL::Rect& getBounds() const;

			//returns the x-index of the Tile's sprite
			int getSprX() const;

			//returns the y-index of the Tile's sprite
			int getSprY() const;

			//returns the ID of the Tile
			int getID() const;

			//serialization operator
			friend std::ostream& operator<<(std::ostream& os,
							const Tile& t);

			//deserialization operator
			friend std::istream& operator>>(std::istream& is,
							Tile& t);

		//private fields and methods
		private:
			//fields
			CPPSDL::Rect bounds; //the bounds of the Tile
			int sprX; //the x-index of the Tile's sprite
			int sprY; //the y-index of the Tile's sprite
			int id; //the Tile's ID
	};
}

//end of header
