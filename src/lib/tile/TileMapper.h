/*
 * TileMapper.h
 * Defines a typedef for a map that maps integers to TSPair objects
 * Created by Andrew Davis
 * Created on 7/10/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <map>
#include "TSPair.h"

//namespace declaration
namespace OrbE {
	//typedef definition
	typedef std::map<int, TSPair> TileMapper;
}

//end of header
