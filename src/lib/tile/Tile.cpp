/*
 * Tile.cpp
 * Implements a class that represents a tile
 * Created by Andrew Davis
 * Created on 6/19/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Tile.h"

//default constructor
OrbE::Tile::Tile()
	: OrbE::Tile(0, 0, 1, 1, 0, 0, 0) //call the first constructor
{
	//no code needed
}

//first constructor
OrbE::Tile::Tile(int x, int y, int w, int h, 
			int newSprX, int newSprY, int newID)
	: OrbE::Tile(CPPSDL::Rect(x, y, w, h), newSprX, newSprY, newID)
{
	//no code needed
}

//second constructor
OrbE::Tile::Tile(const CPPSDL::Rect& newBounds, 
			int newSprX, int newSprY, int newID)
	: bounds(newBounds), sprX(newSprX), sprY(newSprY), id(newID)
{
	//no code needed
}

//destructor
OrbE::Tile::~Tile() {
	//no code needed
}

//copy constructor
OrbE::Tile::Tile(const OrbE::Tile& t)
	: bounds(t.bounds), sprX(t.sprX), sprY(t.sprY), id(t.id)
{
	//no code needed
}

//assignment operator
OrbE::Tile& OrbE::Tile::operator=(const OrbE::Tile& src) {
	this->bounds = src.bounds; //assign the bounding box
	this->sprX = src.sprX; //assign the sprite's x-index
	this->sprY = src.sprY; //assign the sprite's y-index
	this->id = src.id; //assign the sprite's ID
	return *this; //and return the instance
}

//getBounds method - returns the bounding box of the tile
const CPPSDL::Rect& OrbE::Tile::getBounds() const {
	return this->bounds; //return the bounding box
}

//getSprX method - returns the x-index of the Tile's sprite
int OrbE::Tile::getSprX() const {
	return this->sprX; //return the x-index field
}

//getSprY method - returns the y-index of the Tile's sprite
int OrbE::Tile::getSprY() const {
	return this->sprY; //return the y-index field
}

//getID method - returns the Tile's ID
int OrbE::Tile::getID() const {
	return this->id; //return the ID field
}

namespace OrbE {
	//serialization operator
	std::ostream& operator<<(std::ostream& os, const Tile& t) {
		//serialize the bounding rect
		os << t.bounds.getX() << ' ';
		os << t.bounds.getY() << ' ';
		os << t.bounds.getWidth() << ' ';
		os << t.bounds.getHeight() << ' ';

		//serialize the sprite coords
		os << t.sprX << ' ';
		os << t.sprY << ' ';

		//serialize the tile ID
		os << t.id << '\n';

		//and return the stream
		return os;
	}

	//deserialization operator
	std::istream& operator>>(std::istream& is, Tile& t) {
		//deserialize the bounding rect
		int x, y, w, h;
		is >> x;
		is >> y;
		is >> w;
		is >> h;
		t.bounds = CPPSDL::Rect(x, y, w, h);

		//deserialize the sprite coords
		is >> t.sprX;
		is >> t.sprY;

		//deserialize the tile ID
		is >> t.id;

		//and return the stream
		return is;
	}
}

//end of implementation
