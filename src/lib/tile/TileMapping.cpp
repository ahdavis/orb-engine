/*
 * TileMapping.cpp
 * Implements a functor class that maps integers to Tile objects
 * Created by Andrew Davis
 * Created on 7/10/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "TileMapping.h"

//includes
#include <cstdlib>
#include "../except/TileException.h"

//default constructor
OrbE::TileMapping::TileMapping()
	: db(), count(0), tileWidth(1), tileHeight(1), tileX(0), tileY(0)
{
	//no code needed
}

//first main constructor
OrbE::TileMapping::TileMapping(const std::vector<OrbE::TSPair>& newData,
				int newTileWidth, int newTileHeight,
				int newLevelWidth, int newLevelHeight)
	: db(), count(newData.size()), tileWidth(newTileWidth),
		tileHeight(newTileHeight), tileX(0), tileY(0),
		levWidth(newLevelWidth), levHeight(newLevelHeight)
{
	//loop and put the argument vector into the database
	for(std::size_t i = 0; i < newData.size(); i++) {
		this->db[i] = newData[i];
	}
}

//second main constructor
OrbE::TileMapping::TileMapping(const OrbE::TileMapper& newData,
				int newTileWidth, int newTileHeight,
				int newLevelWidth, int newLevelHeight)
	: db(newData), count(newData.size()), tileWidth(newTileWidth),
		tileHeight(newTileHeight), tileX(0), tileY(0),
		levWidth(newLevelWidth), levHeight(newLevelHeight)
{
	//no code needed
}

//destructor
OrbE::TileMapping::~TileMapping() {
	//no code needed
}

//copy constructor
OrbE::TileMapping::TileMapping(const OrbE::TileMapping& tm)
	: db(tm.db), count(tm.count), tileWidth(tm.tileWidth),
		tileHeight(tm.tileHeight), tileX(tm.tileX), 
		tileY(tm.tileY), levWidth(tm.levWidth), 
		levHeight(tm.levHeight)
{
	//no code needed
}

//assignment operator
OrbE::TileMapping& OrbE::TileMapping::operator=(const OrbE::TileMapping&
							src) {
	this->db = src.db; //assign the tile database field
	this->count = src.count; //assign the count field
	this->tileWidth = src.tileWidth; //assign the tile width
	this->tileHeight = src.tileHeight; //assign the tile height
	this->tileX = src.tileX; //assign the mapping x-coord
	this->tileY = src.tileY; //assign the mapping y-coord
	this->levWidth = src.levWidth; //assign the level width
	this->levHeight = src.levHeight; //assign the level height
	return *this; //and return the instance
}

//function call operator
OrbE::Tile OrbE::TileMapping::operator()(int id) {
	//declare a Tile to return
	OrbE::Tile ret;

	//make sure that the tile database contains the argument ID
	if(this->db.count(id) == 0) { //if the ID is not in the database
		//then throw an exception
		throw OrbE::TileException(id);
	} else { //if the ID is in the database
		//then generate the Tile
		ret = OrbE::Tile(this->tileX, this->tileY, 
					this->tileWidth, 
					this->tileHeight, 
					this->db.at(id).first,
					this->db.at(id).second, id);
	}

	//update the x and y coords
	this->tileX += this->tileWidth;

	//handle level sides
	if(this->tileX >= this->levWidth) { //end of level
		//zero the x-coord
		this->tileX = 0;

		//and increase the y-coord
		this->tileY += this->tileHeight;
	}

	//and return the Tile
	return ret;
}

//getTileCount method - returns the number of mappable Tiles
int OrbE::TileMapping::getTileCount() const {
	return this->count; //return the count field
}

//resetMappingCoords method - resets the mapping coords to 0
void OrbE::TileMapping::resetMappingCoords() {
	this->tileX = 0; //zero the x-coord
	this->tileY = 0; //zero the y-coord
}

namespace OrbE {
	//serialization operator
	std::ostream& operator<<(std::ostream& os, const TileMapping& tm) {
		//serialize the tile count
		os << tm.count << '\n';

		//loop and serialize the database
		for(auto it = tm.db.begin(); it != tm.db.end(); ++it) {
			os << it->first << ' ';
			os << it->second.first << ' ';
			os << it->second.second << ' ';
		}

		//serialize the remaining fields
		os << tm.tileWidth << ' ';
		os << tm.tileHeight << ' ';
		os << tm.tileX << ' ';
		os << tm.tileY << ' ';
		os << tm.levWidth << ' ';
		os << tm.levHeight << '\n';

		//and return the stream
		return os;
	}

	//deserialization operator
	std::istream& operator>>(std::istream& is, TileMapping& tm) {
		//deserialize the tile count
		is >> tm.count;

		//loop and deserialize the database
		for(int i = 0; i < tm.count; i++) {
			//declare temporary variables to read into
			int id, sprX, sprY;

			//read into those variables
			is >> id;
			is >> sprX;
			is >> sprY;

			//and put the variables into the database
			tm.db[id] = OrbE::TSPair(sprX, sprY);
		}

		//deserialize the remaining fields
		is >> tm.tileWidth;
		is >> tm.tileHeight;
		is >> tm.tileX;
		is >> tm.tileY;
		is >> tm.levWidth;
		is >> tm.levHeight;

		//and return the stream
		return is;
	}
}

//end of implementation
