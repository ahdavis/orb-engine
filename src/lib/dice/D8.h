/*
 * D8.h
 * Declares a class that represents an eight-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "Die.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class D8 final : public Die 
	{
		//public fields and methods
		public:
			//constructor
			D8();

			//destructor
			~D8();

			//copy constructor
			D8(const D8& de);

			//assignment operator
			D8& operator=(const D8& src);

		//no private fields or methods
	};
}

//end of header
