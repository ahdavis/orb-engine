/*
 * OpNode.cpp
 * Implements a class that represents an operational AST node for 
 * evaluating die rolls
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "OpNode.h"

//includes
#include "NodeType.h"
#include "Die.h"

//constructor
OrbE::OpNode::OpNode(OpType newOpType, 
		const std::shared_ptr<ASTNode>& newLeft,
		const std::shared_ptr<ASTNode>& newRight)
	: OrbE::ASTNode(OrbE::NodeType::OP, newLeft, newRight),
		opType(newOpType)
{
	//no code needed
}

//destructor
OrbE::OpNode::~OpNode() {
	//no code needed
}

//copy constructor
OrbE::OpNode::OpNode(const OrbE::OpNode& on)
	: OrbE::ASTNode(on), opType(on.opType)
{
	//no code needed
}

//assignment operator
OrbE::OpNode& OrbE::OpNode::operator=(const OrbE::OpNode& src) {
	OrbE::ASTNode::operator=(src); //call the superclass assignment op
	this->opType = src.opType; //assign the operation type
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
int OrbE::OpNode::eval() const {
	int res = 0; //will hold the result of the evaluation

	//handle different operations
	switch(this->opType) {
		case OrbE::OpType::ADD: //addition
			{
				//calculate the result
				res = this->left->eval() + 
					this->right->eval();

				break;
			}

		case OrbE::OpType::SUB: //subtraction
			{
				//calculate the result
				res = this->left->eval() -
					this->right->eval();

				break;
			}

		case OrbE::OpType::DIE: //die roll
			{
				//get the multiplier
				int mult = this->left->eval();

				//get the side count
				int sides = this->right->eval();

				//create a Die object
				OrbE::Die die(sides);

				//and roll the die
				for(int i = 0; i < mult; i++) {
					res += die.roll();
				}

				break;
			}
		default:
			{
				break;
			}

	}

	//return the result of the calculations
	return res;
}

//end of implementation
