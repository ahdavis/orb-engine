/*
 * Token.h
 * Declares a class that represents a parser token for evaluating die rolls
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "TokenType.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class Token final {
		//public fields and methods
		public:
			//first constructor
			Token(TokenType newType, int newValue);

			//second constructor
			Token(TokenType newType, char newValue);

			//destructor
			~Token();

			//copy constructor
			Token(const Token& t);

			//assignment operator
			Token& operator=(const Token& src);

			//getter methods
			
			//returns the type of the Token
			TokenType getType() const;

			//returns the Token's value as a character
			char getCharValue() const;

			//returns the Token's value as an int
			int getIntValue() const;

		//private fields and methods
		private:
			//fields
			TokenType type; //the type of the Token
			int value; //the value of the Token
	};
}

//end of header
