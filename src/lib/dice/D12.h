/*
 * D12.h
 * Declares a class that represents a twelve-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "Die.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class D12 final : public Die 
	{
		//public fields and methods
		public:
			//constructor
			D12();

			//destructor
			~D12();

			//copy constructor
			D12(const D12& dt);

			//assignment operator
			D12& operator=(const D12& src);

		//no private fields or methods
	};
}

//end of header
