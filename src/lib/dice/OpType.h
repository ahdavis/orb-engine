/*
 * OpType.h
 * Enumerates types of die operations
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//no includes

//namespace declaration
namespace OrbE {
	//enum definition
	enum class OpType {
		DIE, //die operator
		ADD, //addition operator
		SUB //subtraction operator
	};
}

//end of enum
