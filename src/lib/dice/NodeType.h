/*
 * NodeType.h
 * Enumerates types of die AST nodes
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//no includes

//namespace declaration
namespace OrbE {
	//enum definition
	enum class NodeType {
		NUM, //numeric node
		OP //operation node
	};
}

//end of enum
