/*
 * Lexer.h
 * Declares a class that lexes die equations
 * Created by Andrew Davis
 * Created on 6/21/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <string>
#include "Token.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class Lexer final {
		//public fields and methods
		public:
			//constructor
			explicit Lexer(const std::string& newText);

			//destructor
			~Lexer();

			//copy constructor
			Lexer(const Lexer& l);

			//assignment operator
			Lexer& operator=(const Lexer& src);

			//method
			
			//returns the next token consumed from the text
			Token getNextToken();

		//private fields and methods
		private:
			//methods
			void advance(); //advances the lexer
			void skipWhitespace(); //skips whitespace
			int integer(); //returns an integer from the text

			//fields
			std::string text; //the text to lex
			int pos; //the current lexing position in the text
			char curChar; //the current character being lexed
	};
}

//end of header
