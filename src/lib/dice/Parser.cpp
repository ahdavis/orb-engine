/*
 * Parser.cpp
 * Implements a class that parses die equations
 * Created by Andrew Davis
 * Created on 6/21/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Parser.h"

//includes
#include "../except/ParserException.h"
#include "OpType.h"
#include "TokenType.h"
#include "OpNode.h"
#include "NumNode.h"

//constructor
OrbE::Parser::Parser(const OrbE::Lexer& newLexer)
	: lexer(newLexer), curToken(OrbE::TokenType::EOL, '\0')
{
	//init the current token
	this->curToken = this->lexer.getNextToken();
}

//destructor
OrbE::Parser::~Parser() {
	//no code needed
}

//copy constructor
OrbE::Parser::Parser(const OrbE::Parser& p)
	: lexer(p.lexer), curToken(p.curToken)
{
	//no code needed
}

//assignment operator
OrbE::Parser& OrbE::Parser::operator=(const OrbE::Parser& src) {
	this->lexer = src.lexer; //assign the lexer
	this->curToken = src.curToken; //assign the current token
	return *this; //and return the instance
}

//parse method - parses the die equation
std::shared_ptr<OrbE::ASTNode> OrbE::Parser::parse() {
	return this->expr(); //return the parsed expression
}

//private eat method - processes a token
void OrbE::Parser::eat(OrbE::TokenType type) {
	//verify the token
	if(this->curToken.getType() == type) { //if the types match
		//then update the current token
		this->curToken = this->lexer.getNextToken();
	} else { //no match
		//so throw an exception
		throw OrbE::ParserException();
	}
}

//private factor method - parses a factor
std::shared_ptr<OrbE::ASTNode> OrbE::Parser::factor() {
	OrbE::Token token = this->curToken; //save the current token

	//handle token types
	if(token.getType() == OrbE::TokenType::NUM) { //integer
		this->eat(OrbE::TokenType::NUM); //get the next token

		//and return an AST pointer
		return std::make_shared<OrbE::NumNode>(OrbE::NumNode(
						token.getIntValue()));
	} else if(token.getType() == OrbE::TokenType::LP) { //(
		this->eat(OrbE::TokenType::LP); //get the next token

		//parse the parenthesis
		std::shared_ptr<ASTNode> node = this->expr();

		this->eat(OrbE::TokenType::RP); //get the next token

		return node; //and return the parsed parenthesis
	}

	//throw an exception
	throw OrbE::ParserException();

}

//term method - parses a die operator
std::shared_ptr<OrbE::ASTNode> OrbE::Parser::term() {
	//get the result of the factor method
	std::shared_ptr<OrbE::ASTNode> node = this->factor();

	//loop while the token represents a die operator
	while(this->curToken.getType() == OrbE::TokenType::DIE) {
		//save the token
		OrbE::Token token = this->curToken;
		
		//parse the die operator
		this->eat(OrbE::TokenType::DIE);

		//and recursively generate the AST
		node = std::make_shared<OrbE::OpNode>(OrbE::OpNode(
						OrbE::OpType::DIE,
						node, this->factor()));

	}

	//return the generated AST
	return node;
}

//private expr method - parses addition and subtraction
std::shared_ptr<OrbE::ASTNode> OrbE::Parser::expr() {
	//get the result of the term method
	std::shared_ptr<OrbE::ASTNode> node = this->term();

	//loop while the token type is addition or subtraction
	while((this->curToken.getType() == OrbE::TokenType::ADD) ||
		(this->curToken.getType() == OrbE::TokenType::SUB)) {
		//save the current token
		OrbE::Token token = this->curToken;

		//parse the operators
		if(token.getType() == OrbE::TokenType::ADD) {
			this->eat(OrbE::TokenType::ADD);
		} else if(token.getType() == OrbE::TokenType::SUB) {
			this->eat(OrbE::TokenType::SUB);
		}

		//and generate the AST
		if(token.getType() == OrbE::TokenType::ADD) {
			node = std::make_shared<OrbE::OpNode>(
					OrbE::OpNode(
						OrbE::OpType::ADD,
						node, this->term()));
		} else if(token.getType() == OrbE::TokenType::SUB) {
			node = std::make_shared<OrbE::OpNode>(
					OrbE::OpNode(
						OrbE::OpType::SUB,
						node, this->term()));
		}
	}

	//and return the generated AST
	return node;
}

//end of implementation
