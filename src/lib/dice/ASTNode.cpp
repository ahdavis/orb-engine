/*
 * ASTNode.cpp
 * Implements a class that represents an AST node for evaluating die rolls
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "ASTNode.h"

//constructor
OrbE::ASTNode::ASTNode(OrbE::NodeType newType,
			const std::shared_ptr<OrbE::ASTNode>& newLeft,
			const std::shared_ptr<OrbE::ASTNode>& newRight)
	: type(newType), left(newLeft), right(newRight)
{
	//no code needed
}

//destructor
OrbE::ASTNode::~ASTNode() {
	//no code needed
}

//copy constructor
OrbE::ASTNode::ASTNode(const OrbE::ASTNode& ast)
	: type(ast.type), left(ast.left), right(ast.right)
{
	//no code needed
}

//assignment operator
OrbE::ASTNode& OrbE::ASTNode::operator=(const OrbE::ASTNode& src) {
	this->type = src.type; //assign the type field
	this->left = src.left; //assign the left branch
	this->right = src.right; //assign the right branch
	return *this; //and return the instance
}

//getType method - returns the node's type
OrbE::NodeType OrbE::ASTNode::getType() const {
	return this->type; //return the type field
}

//end of implementation
