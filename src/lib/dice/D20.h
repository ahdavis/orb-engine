/*
 * D20.h
 * Declares a class that represents a twenty-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "Die.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class D20 final : public Die 
	{
		//public fields and methods
		public:
			//constructor
			D20();

			//destructor
			~D20();

			//copy constructor
			D20(const D20& dt);

			//assignment operator
			D20& operator=(const D20& src);

		//no private fields or methods
	};
}

//end of header
