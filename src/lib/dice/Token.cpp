/*
 * Token.cpp
 * Implements a class that represents a parser token 
 * for evaluating die rolls
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Token.h"

//first constructor
OrbE::Token::Token(OrbE::TokenType newType, int newValue)
	: type(newType), value(newValue)
{
	//no code needed
}

//second constructor
OrbE::Token::Token(OrbE::TokenType newType, char newValue)
	: OrbE::Token(newType, static_cast<int>(newValue))
{
	//no code needed
}

//destructor
OrbE::Token::~Token() {
	//no code needed
}

//copy constructor
OrbE::Token::Token(const OrbE::Token& t)
	: type(t.type), value(t.value) 
{
	//no code needed
}

//assignment operator
OrbE::Token& OrbE::Token::operator=(const OrbE::Token& src) {
	this->type = src.type; //assign the type field
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//getType method - returns the type of the Token
OrbE::TokenType OrbE::Token::getType() const {
	return this->type; //return the type field
}

//getIntValue method - returns the value of the Token as an int
int OrbE::Token::getIntValue() const {
	return this->value; //return the value field
}

//getCharValue method - returns the value of the Token as a character
char OrbE::Token::getCharValue() const {
	return static_cast<char>(this->value); //return the value field
}

//end of implementation
