/*
 * D20.cpp
 * Implements a class that represents a twenty-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "D20.h"

//constructor
OrbE::D20::D20()
	: OrbE::Die(20) //call the superclass constructor
{
	//no code needed
}

//destructor
OrbE::D20::~D20() {
	//no code needed
}

//copy constructor
OrbE::D20::D20(const OrbE::D20& dt)
	: OrbE::Die(dt) //call the superclass copy constructor
{
	//no code needed
}

//assignment operator
OrbE::D20& OrbE::D20::operator=(const OrbE::D20& src) {
	OrbE::Die::operator=(src); //call the superclass assignment op
	return *this; //and return the instance
}

//end of implementation
