/*
 * evalDieEqn.h
 * Declares functions that evaluate die equations
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include <string>

//namespace declaration
namespace OrbE {
	//evaluates a die equation stored in a std::string
	int evalDieEqn(const std::string& eqn); 

	//evaluates a die equation stored in a C string
	int evalDieEqn(const char* eqn); 
}

//end of header
