/*
 * TokenType.h
 * Enumerates die roll parser tokens
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//no includes

//namespace declaration
namespace OrbE {
	//enum definition
	enum class TokenType {
		EOL, //end of line token
		NUM, //number token
		LP, //left parenthesis token
		RP, //right parenthesis token
		ADD, //addition token
		SUB, //subtraction token
		DIE //die operator token
	};
}

//end of enum
