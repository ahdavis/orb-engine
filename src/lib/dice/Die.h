/*
 * Die.h
 * Declares a class that represents a multisided die
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//no includes

//namespace declaration
namespace OrbE {
	//class declaration
	class Die {
		//public fields and methods
		public:
			//default constructor
			Die();

			//main constructor
			explicit Die(int newSideCount);

			//destructor
			virtual ~Die();

			//copy constructor
			Die(const Die& d);

			//assignment operator
			Die& operator=(const Die& src);

			//getter methods
			
			//returns the side count of the Die
			int getSideCount() const;

			//returns the value of the Die
			int getValue() const;

			//setter method
			
			//sets the side count of the Die
			void setSideCount(int newSideCount);

			//other method
			
			//rolls the Die and returns the new value
			int roll();

		//private fields and methods
		private:
			//constant
			
			//the default side count
			static const int DEFAULT_SIDE_CT; 

			//fields
			int sideCount; //the number of sides on the Die
			int value; //the value of the Die
	};
}

//end of header
