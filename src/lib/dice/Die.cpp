/*
 * Die.cpp
 * Implements a class that represents a multisided die
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Die.h"

//includes
#include <cstdlib>
#include <ctime>

//constant initialization
const int OrbE::Die::DEFAULT_SIDE_CT = 6; //defaults to a six-sided die

//default constructor
OrbE::Die::Die()
	: OrbE::Die(DEFAULT_SIDE_CT) //call the other constructor
{
	//no code needed
}

//main constructor
OrbE::Die::Die(int newSideCount)
	: sideCount(6), value(1) //init the fields with dummy values
{
	std::srand(std::time(NULL)); //seed the RNG
	this->setSideCount(newSideCount); //update the side count
	this->roll(); //and roll the die
}


//destructor
OrbE::Die::~Die() {
	//no code needed
}

//copy constructor
OrbE::Die::Die(const OrbE::Die& d)
	: sideCount(d.sideCount), value(d.value) //copy the fields
{
	//no code needed
}

//assignment operator
OrbE::Die& OrbE::Die::operator=(const OrbE::Die& src) {
	this->sideCount = src.sideCount; //assign the side count
	this->value = src.value; //assign the value
	return *this; //and return the instance
}

//getSideCount method - returns the number of sides on the Die
int OrbE::Die::getSideCount() const {
	return this->sideCount; //return the side count field
}

//getValue method - returns the value of the Die
int OrbE::Die::getValue() const {
	return this->value; //return the value field
}

//setSideCount method - sets the number of sides on the Die
void OrbE::Die::setSideCount(int newSideCount) {
	if(newSideCount < 2) { //if an invalid number of sides was given
		this->sideCount = 2; //then set the side count to 2
	} else { //valid side count
		this->sideCount = newSideCount; //set the side count
	}
}

//roll method - rolls the Die and returns the new value
int OrbE::Die::roll() {
	this->value = (std::rand() % this->sideCount) + 1; //roll the die
	return this->value; //and return the new value
}

//end of implementation
