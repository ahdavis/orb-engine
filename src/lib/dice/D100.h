/*
 * D100.h
 * Declares a class that represents a one hundred-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "Die.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class D100 final : public Die 
	{
		//public fields and methods
		public:
			//constructor
			D100();

			//destructor
			~D100();

			//copy constructor
			D100(const D100& dh);

			//assignment operator
			D100& operator=(const D100& src);

		//no private fields or methods
	};
}

//end of header
