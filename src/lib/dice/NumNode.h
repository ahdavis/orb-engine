/*
 * NumNode.h
 * Declares a class that represents a numeric AST node for 
 * evaluating die rolls
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "ASTNode.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class NumNode final : public ASTNode
	{
		//public fields and methods
		public:
			//constructor
			explicit NumNode(int newValue);

			//destructor
			~NumNode();

			//copy constructor
			NumNode(const NumNode& nn);

			//assignment operator
			NumNode& operator=(const NumNode& src);

			//evaluates the node
			int eval() const override;

		//private fields and methods
		private:
			//field
			int value; //the value of the Node
	};
}

//end of header
