/*
 * D10.cpp
 * Implements a class that represents a ten-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "D10.h"

//constructor
OrbE::D10::D10()
	: OrbE::Die(10) //call the superclass constructor
{
	//no code needed
}

//destructor
OrbE::D10::~D10() {
	//no code needed
}

//copy constructor
OrbE::D10::D10(const OrbE::D10& dt)
	: OrbE::Die(dt) //call the superclass copy constructor
{
	//no code needed
}

//assignment operator
OrbE::D10& OrbE::D10::operator=(const OrbE::D10& src) {
	OrbE::Die::operator=(src); //call the superclass assignment op
	return *this; //and return the instance
}

//end of implementation
