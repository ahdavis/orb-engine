/*
 * Lexer.cpp
 * Implements a class that lexes die equations
 * Created by Andrew Davis
 * Created on 6/21/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Lexer.h"

//includes
#include <sstream>
#include "../except/LexerException.h"
#include "TokenType.h"
#include "../util/functions.h"

//constructor
OrbE::Lexer::Lexer(const std::string& newText)
	: text(newText), pos(0), curChar('\0')
{
	//init the current character
	this->curChar = this->text[this->pos];
}

//destructor
OrbE::Lexer::~Lexer() {
	//no code needed
}	

//copy constructor
OrbE::Lexer::Lexer(const OrbE::Lexer& l)
	: text(l.text), pos(l.pos), curChar(l.curChar)
{
	//no code needed
}

//assignment operator
OrbE::Lexer& OrbE::Lexer::operator=(const OrbE::Lexer& src) {
	this->text = src.text; //assign the text field
	this->pos = src.pos; //assign the position field
	this->curChar = src.curChar; //assign the current character
	return *this; //and return the instance
}

//getNextToken method - returns a token consumed from the text
OrbE::Token OrbE::Lexer::getNextToken() {
	//loop until EOL
	while(this->curChar != '\0') {
		//handle different characters
		if(OrbE::isSpace(this->curChar)) { //space
			this->skipWhitespace(); //skip the space
			continue; //and restart the loop
		}

		if(OrbE::isDigit(this->curChar)) { //integer
			//return an integer token
			return OrbE::Token(OrbE::TokenType::NUM, 
					this->integer());
		}

		if(this->curChar == '(') { //left parenthesis
			//advance the lexer
			this->advance();

			//and return a left parenthesis token
			return OrbE::Token(OrbE::TokenType::LP, '(');
		}
		
		if(this->curChar == ')') { //right parenthesis
			//advance the lexer
			this->advance();

			//and return a right parenthesis token
			return OrbE::Token(OrbE::TokenType::RP, ')');
		}

		if((this->curChar == 'd') || 
				(this->curChar == 'D')) { //die operator
			//save the current character
			char tmp = this->curChar;
			
			//advance the lexer
			this->advance();

			//and return a die operator token
			return OrbE::Token(OrbE::TokenType::DIE, tmp);
		}

		if(this->curChar == '+') { //addition operator
			//advance the lexer
			this->advance();

			//and return an addition token
			return OrbE::Token(OrbE::TokenType::ADD, '+');
		}

		if(this->curChar == '-') { //subtraction operator
			//advance the lexer
			this->advance();

			//and return a subtraction token
			return OrbE::Token(OrbE::TokenType::SUB, '-');
		}
			

		//no match, so throw an exception
		throw OrbE::LexerException(this->curChar);
	}

	//return an EOL token
	return OrbE::Token(OrbE::TokenType::EOL, '\0');
}

//private advance method - advances the lexer position
void OrbE::Lexer::advance() {
	this->pos++; //increment the position index

	//handle end of text
	if(this->pos > (static_cast<int>(this->text.length()) - 1)) {
		this->curChar = '\0'; //null the current character
	} else {
		this->curChar = this->text[this->pos];
	}
}

//private skipWhitespace method - skips whitespace
void OrbE::Lexer::skipWhitespace() {
	//loop through the whitespace
	while((this->curChar != '\0') && (OrbE::isSpace(this->curChar))) {
		this->advance();
	}
}

//private integer method - returns an integer consumed from the text
int OrbE::Lexer::integer() {
	std::stringstream ss; //used to assemble the integer string

	//loop through the integer
	while((this->curChar != '\0') && (OrbE::isDigit(this->curChar))) {
		ss << this->curChar; //add the current char to the stream
		this->advance(); //and move to the next character
	}

	//and return the stream as an integer
	std::string tmp = ss.str();
	return std::stoi(tmp);
}

//end of implementation
