/*
 * ASTNode.h
 * Declares a class that represents an AST node for evaluating die rolls
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include <memory>
#include "NodeType.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class ASTNode {
		//public fields and methods
		public:
			//constructor
			ASTNode(NodeType newType, 
				const std::shared_ptr<ASTNode>& newLeft,
				const std::shared_ptr<ASTNode>& newRight);

			//destructor
			virtual ~ASTNode();

			//copy constructor
			ASTNode(const ASTNode& ast);

			//assignment operator
			ASTNode& operator=(const ASTNode& src);

			//getter method
			
			//returns the type of the node
			NodeType getType() const;

			//other method
			
			//evaluates the node
			virtual int eval() const = 0;

		//protected fields and methods
		protected:
			//fields
			NodeType type; //the type of the node
			std::shared_ptr<ASTNode> left; //the left branch
			std::shared_ptr<ASTNode> right; //the right branch
	};
}

//end of header
