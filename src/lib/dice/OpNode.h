/*
 * OpNode.h
 * Declares a class that represents an operational AST node for 
 * evaluating die rolls
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <memory>
#include "OpType.h"
#include "ASTNode.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class OpNode final : public ASTNode
	{
		//public fields and methods
		public:
			//constructor
			OpNode(OpType newOpType, 
				const std::shared_ptr<ASTNode>& newLeft,
				const std::shared_ptr<ASTNode>& newRight);

			//destructor
			~OpNode();

			//copy constructor
			OpNode(const OpNode& on);

			//assignment operator
			OpNode& operator=(const OpNode& src);

			//evaluates the node
			int eval() const override;

		//private fields and methods
		private:
			//field
			OpType opType; //the operation type
	};
}

//end of header
