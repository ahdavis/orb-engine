/*
 * Parser.h
 * Declares a class that parses die equations
 * Created by Andrew Davis
 * Created on 6/21/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <memory>
#include "Lexer.h"
#include "Token.h"
#include "TokenType.h"
#include "ASTNode.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class Parser final {
		//public fields and methods
		public:
			//constructor
			explicit Parser(const Lexer& newLexer);

			//destructor
			~Parser();

			//copy constructor
			Parser(const Parser& p);

			//assignment operator
			Parser& operator=(const Parser& src);

			//parses the equation
			std::shared_ptr<ASTNode> parse();

		//private fields and methods
		private:
			//methods
			void eat(TokenType type); //processes a token

			//parses a factor
			std::shared_ptr<ASTNode> factor(); 

			//parses a die operator
			std::shared_ptr<ASTNode> term();

			//parses addition and subtraction
			std::shared_ptr<ASTNode> expr();

			//fields
			Lexer lexer; //the symbol lexer
			Token curToken; //the current token being parsed
	};
}

//end of header
