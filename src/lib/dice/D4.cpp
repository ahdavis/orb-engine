/*
 * D4.cpp
 * Implements a class that represents a four-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "D4.h"

//constructor
OrbE::D4::D4()
	: OrbE::Die(4) //call the superclass constructor
{
	//no code needed
}

//destructor
OrbE::D4::~D4() {
	//no code needed
}

//copy constructor
OrbE::D4::D4(const OrbE::D4& df)
	: OrbE::Die(df) //call the superclass copy constructor
{
	//no code needed
}

//assignment operator
OrbE::D4& OrbE::D4::operator=(const OrbE::D4& src) {
	OrbE::Die::operator=(src); //call the superclass assignment op
	return *this; //and return the instance
}

//end of implementation
