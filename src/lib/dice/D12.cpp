/*
 * D12.cpp
 * Implements a class that represents a twelve-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "D12.h"

//constructor
OrbE::D12::D12()
	: OrbE::Die(12) //call the superclass constructor
{
	//no code needed
}

//destructor
OrbE::D12::~D12() {
	//no code needed
}

//copy constructor
OrbE::D12::D12(const OrbE::D12& dt)
	: OrbE::Die(dt) //call the superclass copy constructor
{
	//no code needed
}

//assignment operator
OrbE::D12& OrbE::D12::operator=(const OrbE::D12& src) {
	OrbE::Die::operator=(src); //call the superclass assignment op
	return *this; //and return the instance
}

//end of implementation
