/*
 * D6.cpp
 * Implements a class that represents a six-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "D6.h"

//constructor
OrbE::D6::D6()
	: OrbE::Die(6) //call the superclass constructor
{
	//no code needed
}

//destructor
OrbE::D6::~D6() {
	//no code needed
}

//copy constructor
OrbE::D6::D6(const OrbE::D6& ds)
	: OrbE::Die(ds) //call the superclass copy constructor
{
	//no code needed
}

//assignment operator
OrbE::D6& OrbE::D6::operator=(const OrbE::D6& src) {
	OrbE::Die::operator=(src); //call the superclass assignment op
	return *this; //and return the instance
}

//end of implementation
