/*
 * D100.cpp
 * Implements a class that represents a one hundred-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "D100.h"

//constructor
OrbE::D100::D100()
	: OrbE::Die(100) //call the superclass constructor
{
	//no code needed
}

//destructor
OrbE::D100::~D100() {
	//no code needed
}

//copy constructor
OrbE::D100::D100(const OrbE::D100& dh)
	: OrbE::Die(dh) //call the superclass copy constructor
{
	//no code needed
}

//assignment operator
OrbE::D100& OrbE::D100::operator=(const OrbE::D100& src) {
	OrbE::Die::operator=(src); //call the superclass assignment op
	return *this; //and return the instance
}

//end of implementation
