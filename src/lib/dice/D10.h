/*
 * D10.h
 * Declares a class that represents a ten-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "Die.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class D10 final : public Die 
	{
		//public fields and methods
		public:
			//constructor
			D10();

			//destructor
			~D10();

			//copy constructor
			D10(const D10& dt);

			//assignment operator
			D10& operator=(const D10& src);

		//no private fields or methods
	};
}

//end of header
