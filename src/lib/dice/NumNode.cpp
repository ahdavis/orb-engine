/*
 * NumNode.cpp
 * Implements a class that represents a numeric AST node for 
 * evaluating die rolls
 * Created by Andrew Davis
 * Created on 6/20/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "NumNode.h"

//include
#include "NodeType.h"

//constructor
OrbE::NumNode::NumNode(int newValue)
	: OrbE::ASTNode(OrbE::NodeType::NUM, nullptr, nullptr),
		value(newValue)
{
	//no code needed
}

//destructor
OrbE::NumNode::~NumNode() {
	//no code needed
}

//copy constructor
OrbE::NumNode::NumNode(const OrbE::NumNode& nn)
	: OrbE::ASTNode(nn), value(nn.value)
{
	//no code needed
}

//assignment operator
OrbE::NumNode& OrbE::NumNode::operator=(const OrbE::NumNode& src) {
	OrbE::ASTNode::operator=(src); //call the superclass assignment op
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
int OrbE::NumNode::eval() const {
	return this->value; //return the value field
}

//end of header
