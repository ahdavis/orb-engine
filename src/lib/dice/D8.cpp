/*
 * D8.cpp
 * Implements a class that represents an eight-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "D8.h"

//constructor
OrbE::D8::D8()
	: OrbE::Die(8) //call the superclass constructor
{
	//no code needed
}

//destructor
OrbE::D8::~D8() {
	//no code needed
}

//copy constructor
OrbE::D8::D8(const OrbE::D8& de)
	: OrbE::Die(de) //call the superclass copy constructor
{
	//no code needed
}

//assignment operator
OrbE::D8& OrbE::D8::operator=(const OrbE::D8& src) {
	OrbE::Die::operator=(src); //call the superclass assignment op
	return *this; //and return the instance
}

//end of implementation
