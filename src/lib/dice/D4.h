/*
 * D4.h
 * Declares a class that represents a four-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "Die.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class D4 final : public Die 
	{
		//public fields and methods
		public:
			//constructor
			D4();

			//destructor
			~D4();

			//copy constructor
			D4(const D4& df);

			//assignment operator
			D4& operator=(const D4& src);

		//no private fields or methods
	};
}

//end of header
