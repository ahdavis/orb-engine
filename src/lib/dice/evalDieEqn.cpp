/*
 * evalDieEqn.cpp
 * Implements functions that evaluate die equations
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "evalDieEqn.h"

//includes
#include <memory>
#include "ASTNode.h"
#include "Lexer.h"
#include "Parser.h"

//first evalDieEqn function - evaluates a die equation 
//stored in a std::string
int OrbE::evalDieEqn(const std::string& eqn) {
	//create objects to parse the equation
	OrbE::Lexer lexer(eqn); //create a lexer
	OrbE::Parser parser(lexer); //create a parser

	//parse the equation and get its AST
	std::shared_ptr<OrbE::ASTNode> ast = parser.parse();

	//and return the result of evaluating the AST
	return ast->eval();
}

//second evalDieEqn function - evaluates a die equation 
//stored in a C string
int OrbE::evalDieEqn(const char* eqn) {
	//call the other function
	return OrbE::evalDieEqn(std::string(eqn)); 
}

//end of implementation
