/*
 * D6.h
 * Declares a class that represents a six-sided die
 * Created by Andrew Davis
 * Created on 7/12/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include "Die.h"

//namespace declaration
namespace OrbE {
	//class declaration
	class D6 final : public Die 
	{
		//public fields and methods
		public:
			//constructor
			D6();

			//destructor
			~D6();

			//copy constructor
			D6(const D6& ds);

			//assignment operator
			D6& operator=(const D6& src);

		//no private fields or methods
	};
}

//end of header
